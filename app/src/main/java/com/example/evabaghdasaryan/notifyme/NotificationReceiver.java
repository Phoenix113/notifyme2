package com.example.evabaghdasaryan.notifyme;

/**
 * Created by evabaghdasaryan on 9/21/17.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.Date;
import java.util.UUID;


public class NotificationReceiver extends BroadcastReceiver{
    private DatabaseHandler db;


    @Override
    public void onReceive(Context context, Intent intent) {
        db = new DatabaseHandler(context);

        showNotification(context, intent.getIntExtra("ID", -1), intent.getStringExtra("Message"), intent.getLongExtra("Date", 0));
    }

    private void showNotification(Context context, int id, String message, long date) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("Notification!") // title for notification
                .setContentText(message)
                .setAutoCancel(true);// clear notification after click

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), builder.build());

        Notification notification = new Notification(id, message, date);
        db.deleteNotification(notification);

    }
}