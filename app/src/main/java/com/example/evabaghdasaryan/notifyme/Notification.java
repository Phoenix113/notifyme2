package com.example.evabaghdasaryan.notifyme;

/**
 * Created by evabaghdasaryan on 9/20/17.
 */


public class Notification {

    //private variables
    private int id;
    private String message;
    protected long dateTime;

    // Empty constructor
    public Notification(){

    }
    // constructor
    public Notification(int id, String message, long dateTime){
        this.id = id;
        this.message = message;
        this.dateTime = dateTime;
    }

    // constructor
    public Notification(String message, long dateTime){
        this.message = message;
        this.dateTime = dateTime;
    }
    // getting ID
    public int getID(){
        return this.id;
    }

    // setting id
    public void setID(int id){
        this.id = id;
    }

    // getting name
    public String getMessage(){
        return this.message;
    }

    // setting name
    public void setMessage(String message){
        this.message = message;
    }

    // getting phone number
    public long getDateTime(){
        return this.dateTime;
    }

    // setting phone number
    public void setDateTime(long phone_number){
        this.dateTime = phone_number;
    }
}