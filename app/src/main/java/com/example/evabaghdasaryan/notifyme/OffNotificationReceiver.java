package com.example.evabaghdasaryan.notifyme;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import static com.example.evabaghdasaryan.notifyme.MainActivity.getDate;

/**
 * Created by evabaghdasaryan on 9/22/17.
 */

public class OffNotificationReceiver extends BroadcastReceiver {
    private DatabaseHandler db;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent i = new Intent(context, NotificationService.class);
            ComponentName service = context.startService(i);
            Log.d("HERE", "onReceive: ");

        }

    }

}
