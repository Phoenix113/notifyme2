package com.example.evabaghdasaryan.notifyme;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.BulletSpan;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends Activity {

    private EditText txtDate;
    private EditText txtMessage;
    private SimpleDateFormat sdf;
    private Calendar notificationDate;
    private Calendar notificationInitial;
    private DatabaseHandler db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtDate = (EditText) findViewById(R.id.in_date);
        txtMessage = (EditText)findViewById(R.id.message_text);
        db = new DatabaseHandler(this);
    }

    public void saveAll(View view) {
        if (notificationDate != null && !isRepeated(notificationDate.getTimeInMillis(), txtMessage.getText().toString())){
            long mill = notificationDate.getTimeInMillis();
            String message = txtMessage.getText().toString();
            int id = db.getAllNotifications().size();
            Notification notification = new Notification(id, message, mill);
            db.addNotification(notification);
            setUpNotification(notification);
        }
    }

    public void pickDateAndTime(View view) {
        // Get Current Date
        notificationDate = null;
        notificationInitial = Calendar.getInstance();
        sdf = new SimpleDateFormat("yyyy/MM/dd, hh:mm");

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                notificationInitial.set(year, monthOfYear, dayOfMonth);

                // Launch Time Picker Dialog
                new TimePickerDialog(MainActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                notificationInitial.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                notificationInitial.set(Calendar.MINUTE, minute);
                                notificationDate = Calendar.getInstance();
                                notificationDate.set(notificationInitial.get(Calendar.YEAR),
                                        notificationInitial.get(Calendar.MONTH),
                                        notificationInitial.get(Calendar.DAY_OF_MONTH));
                                notificationDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                notificationDate.set(Calendar.MINUTE, minute);
                                notificationDate.set(Calendar.SECOND, 0);
                                txtDate.setText(sdf.format(notificationDate.getTime()));
                            }
                        }, notificationInitial.get(Calendar.HOUR_OF_DAY), notificationInitial.get(Calendar.MINUTE), false)
                        .show();

            }
        }, notificationInitial.get(Calendar.YEAR), notificationInitial.get(Calendar.MONTH),
                notificationInitial.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    public static String getDate(long milliSeconds, String dateFormat) {

        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public boolean isRepeated(long date, String message) {
        for (Notification notifications : db.getAllNotifications()) {
            if(notifications.getMessage().toString().equals(message) &&
                    notifications.getDateTime() == date ) {
                return true;
            }
        }
        return false;
    }

    public void setUpNotification(Notification notification) {
        int id = notification.getID();
        String message = notification.getMessage();
        long date = notification.getDateTime();
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, NotificationReceiver.class);
        intent.putExtra("Message", message);
        intent.putExtra("Date", date);
        intent.putExtra("ID", id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id,  intent, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, date, pendingIntent);
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, date, pendingIntent);
        else
            alarmManager.set(AlarmManager.RTC_WAKEUP, date, pendingIntent);
    }

    public void removeDB(View view) {
        db.removeAll();
    }
}
