package com.example.evabaghdasaryan.notifyme;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by evabaghdasaryan on 9/22/17.
 */

public class NotificationService extends IntentService {
    private DatabaseHandler db;
    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        db = new DatabaseHandler(getApplicationContext());

        for (int i = 1; i <= db.getAllNotifications().size(); i++) {
            long d = db.getNotification(i).getDateTime();
            String m = db.getNotification(i).getMessage();
            int id = i;
            setUpNotification(id, m, d);
        }
    }

    public void setUpNotification(int id, String message, long date) {
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), NotificationReceiver.class);
        intent.putExtra("Message", message);
        intent.putExtra("Date", date);
        intent.putExtra("ID", id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), id,  intent, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, date, pendingIntent);
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, date, pendingIntent);
        else
            alarmManager.set(AlarmManager.RTC_WAKEUP, date, pendingIntent);
    }

}
